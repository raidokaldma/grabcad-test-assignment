GrabCAD test assignment
=======================

Prerequisites
-------------
* Ruby 2.0.0
* Rails 4.0.0
* Postgre 9.2 (probably older versions are also fine)

Cloning project
---------------
```
git clone https://bitbucket.org/raidokaldma/grabcad-test-assignment.git
```

Database setup (development mode)
---------------------------------
Create database user with following credentials `postgres/postgres`.  
Make sure that database with name `grabcad_test_assignment` exists (or create one).  

Settings used for creating a connection:
```
development:
  adapter: postgresql
  encoding: unicode
  database: grabcad_test_assignment
  pool: 5
  username: postgres
  password: postgres
```

Database migration
------------------
Before use, tables and indexes need to be created:  
```
rake db:migrate
```

Database seeding
----------------
Set up initial data - users and privileges:  
```
rake db:seed
```

Starting the server in development mode
---------------------------------------
```
rails server
```

Using application
=================
Application URL: [http://localhost:3000](http://localhost:3000)  
You can log in with users:                                          
`admin/admin` - User with `admin` privilege  
`user/user` - User with no privileges  