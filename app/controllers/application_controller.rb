class ApplicationController < ActionController::Base
  include Security::AuthenticationHelper
  layout 'content_with_menu'

  # Prevent CSRF attacks by raising an exception.
  protect_from_forgery with: :exception

  before_action :setup_log_context, :require_login

private
  #Redirects user to login page if user is not logged in
  def require_login
    unless logged_in?
      redirect_to root_path
    end
  end

  #Adds tags to each log message - username and browser information
  def setup_log_context
    logger.push_tags logged_in_username
    logger.push_tags get_browser
  end

  def get_browser
    request.env['HTTP_USER_AGENT']
  end
end
