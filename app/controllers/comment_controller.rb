class CommentController < ApplicationController
  include CommentHelper

  before_action :check_destroy_permission, only: :destroy

  def index
    @comments = Comment.all
  end

  def destroy
    comment = Comment.find_by_id(params[:id])

    if comment
      comment.destroy!
      flash[:notice] = I18n.t 'comment_deleted'
    end

    redirect_to comment_index_path
  end

  def create
    user = User.find_by_id(logged_in_user_id)
    new_comment = user.comments.create(params[:new_comment].permit(:comment))

    if new_comment.valid?
      flash[:notice] = I18n.t 'comment_added'
    else
      flash[:error] = new_comment.errors.full_messages.to_sentence
    end

    redirect_to comment_index_path
  end

  private
  #Checks if user is authorized to delete the comment
  def check_destroy_permission
    comment = Comment.find_by_id(params[:id])
    unless can_delete? comment
      logger.warn 'User tried to delete someone else\'s comment'
      redirect_to comment_index_path
    end
  end
end
