class Security::AuthenticationController < ApplicationController
  include Security::AuthenticationHelper

  layout 'application'

  skip_before_filter :require_login, only: [:show_login_page, :login]

  def show_login_page
    if logged_in?
      redirect_to comment_index_path
    end
  end

  def login
    param_username = params[:authentication][:username]
    param_password = params[:authentication][:password]

    user = User.find_by_username(param_username.downcase)
    if user && user.authenticate(param_password)
      handle_successful_login(user)
    else
      handle_failed_login(param_username)
    end
  end

  def logout
    flash[:notice] = I18n.t 'logout_notice'
    reset_session
    logger.info 'User logged out'
    redirect_to root_path
  end

  private
  def handle_successful_login(user)
    remember_logged_in_user user
    logger.info "Login successful, username: #{logged_in_username}"
    redirect_to comment_index_path
  end

  def handle_failed_login(param_username)
    flash[:error] = I18n.t 'login_error'
    logger.warn "Login failed, tried username: #{param_username}"
    redirect_to root_path
  end
end
