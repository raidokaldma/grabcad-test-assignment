module Security::AuthenticationHelper
  def logged_in_user_id
    session[:logged_in_user_id]
  end

  def logged_in_username
    session[:logged_in_username]
  end

  def logged_in_user
    #noinspection RailsChecklist05
    User.find_by_id logged_in_user_id
  end

  def logged_in?
    !!logged_in_user_id
  end

  def remember_logged_in_user(user)
    session[:logged_in_user_id] = user.id
    session[:logged_in_username] = user.username
  end
end
