module CommentHelper
  def can_delete?(comment)
    comment.is_author?(logged_in_user_id) || logged_in_user.is_admin?
  end
end
