class Comment < ActiveRecord::Base
  validates_presence_of :comment
  belongs_to :user

  def is_author?(user_id)
    self.user.id == user_id
  end
end
