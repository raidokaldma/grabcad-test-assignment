class User < ActiveRecord::Base
  has_and_belongs_to_many :privileges
  has_many :comments
  has_secure_password

  before_save { username.downcase! }
  validates :username, presence: true, uniqueness: {case_sensitive: false}

  def is_admin?
    admin_privileges = self.privileges.select { |p| p.name == Privilege::ADMIN }
    !admin_privileges.empty?
  end
end
