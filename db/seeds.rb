#Regular user
User.create(username: 'user', password: 'user', password_confirmation: 'user')

#Admin user with admin privilege
admin = User.create(username: 'admin', password: 'admin', password_confirmation: 'admin')
admin.privileges.create(name: 'admin')

