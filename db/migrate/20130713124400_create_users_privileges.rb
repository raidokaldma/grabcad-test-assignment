class CreateUsersPrivileges < ActiveRecord::Migration
  def change
    create_table :privileges_users, :id => false do |t|
      t.references :privilege
      t.references :user
    end
  end
end
