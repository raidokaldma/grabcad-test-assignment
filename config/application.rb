require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module GrabcadTestAssignment
  class Application < Rails::Application
    #Prepends log messages with time and request path (resource)
    config.log_tags = [lambda { |req| Time.now}, :fullpath]
  end
end
