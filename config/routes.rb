GrabcadTestAssignment::Application.routes.draw do
  #Authentication
  root 'security/authentication#show_login_page'
  post '/login' => 'security/authentication#login'
  get '/logout' => 'security/authentication#logout'

  #Comment resource
  resources :comment, only: [:index, :create, :destroy]
end
